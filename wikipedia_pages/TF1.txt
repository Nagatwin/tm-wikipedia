TF1 (té effe un pronounced [te ɛf œ̃]) is a French free-to-air television channel, controlled by TF1 Group, whose major share-holder is Bouygues. TF1's average market share of 24% makes it the most popular domestic network. Flagship series broadcast on the channel include CSI, The Voice and House M.D.
TF1 is part of the TF1 Group of mass media companies, which also includes the news channel LCI. It used to own the satellite TV provider TPS, which have been sold to the Canal+ Group.
The network is a supporter of the Hybrid Broadcast Broadband TV (HbbTV) initiative that is promoting and establishing an open European standard for hybrid set-top boxes for the reception of terrestrial TV and broadband multimedia applications with a single user interface.


== History ==
It was the only channel in France for 28 years, and has often changed its name since the creation of Radio-PTT Vision on 26 April 1935, making it one of the oldest television stations in the world, and one of the very few prewar television stations to remain in existence to the present day. It became Radiodiffusion nationale Télévision (RN Télévision) in 1937, Fernsehsender Paris (Paris Television) during German occupation in 1943, RDF Télévision française in 1944, RTF Télévision in 1949, la Première chaîne de la RTF in 1963 following the creation of the second channel, la Première chaîne de l'ORTF in 1964 and finally Télévision Française 1 (TF1) in 1975.


=== Radio-PTT Vision (1935–1937) ===
Radio-PTT Vision began operations on 26 April 1935 as the first television station in France, using a 30-line mechanical television system based on the Nipkow disk. It was operated by the French PTT agency with a transmitter located atop the Eiffel Tower, and was on air three days a week from 11 am to 11:30 am and 8 pm to 8:30 pm and on Sundays from 5:30 pm to 7:30 pm. On 4 January 1937 the broadcasting hours were changed such that television programmes were aired from 5 pm until 10 pm Wednesdays to Fridays, and from 4 pm to 8:30 pm or 9 pm Saturdays to Tuesdays.


=== Radiodiffusion nationale Télévision (1937–1939) ===
Following successful trials of a "high-definition" 455-line electronic television system designed by Thomson-Houston which improved on the 405-line system originally designed by EMI-Marconi, Radio-PTT Vision renamed itself as Radiodiffusion nationale Télévision (RN Télévision) in July 1937. However, broadcasts using the Nipkow disk system continued alongside the new electronic system until 10 April 1938. In July 1938, a decree of the French PTT agency fixed the French terrestrial television standard as transmitting on 455 lines VHF (46 MHz, positive modulation, 25 frames per second), to be adopted throughout France within three years. The adoption of the electronic standard marked the end of mechanical television in France, and the advent of electronic television to obtain much better image quality. RN Télévision abruptly stopped broadcasts on 3 September 1939 following the entry of France into the Second World War.


=== Fernsehsender Paris (1943–1944) ===
Television broadcasts resumed in occupied France on 7 May 1943 as Fernsehsender Paris, under the control of the Oberkommando der Wehrmacht. It was on air in German and French four days a week from 10 am to noon, three days a week from 3 pm to 8 pm and every evening from 8:30 pm to 10 pm. Fernsehsender Paris stopped broadcasts on 12 August 1944, one week before the liberation of Paris.


=== RDF Télévision française (1944–1949) ===
Television broadcasts in France resumed on 1 October 1944 under the name Télévision française, and following the creation of Radiodiffusion française on 23 March 1945 the television service was renamed as RDF Télévision française. Following the return of the Eiffel Tower to the French after being in American administration following the liberation of Paris, on 1 October 1945 the official resumption of television broadcasts took place with one hour of programming each day. On 20 November 1948, the Secretary of State for Information, François Mitterrand decreed the adoption of the 819-line high-definition VHF standard, which was in use from 1949 until 1981.


=== RTF Télévision (1949–1964) ===
Radiodiffusion française was renamed as Radiodiffusion-télévision française (RTF) on 9 February 1949, and thus began the growth of television as an accepted mass medium in France. On 29 May 1949 the first news programme aired on RTF TV, and on 30 July 1949 a television licence fee was introduced. Residents living outside Paris could view RTF TV for the first time in February 1952 when Télé Lille (now known as France 3 Nord-Pas-de-Calais), a regional broadcaster operating since 10 April 1950 was co-opted into the RTF TV network and became RTF's first relay outside Paris.


=== Première chaîne de l'ORTF (1964–1975) ===
Following the creation of RTF Télévision 2 (now France 2) in 1963, the first channel was renamed as Première chaîne de la RTF (First Channel of the RTF), which was renamed as Première chaîne de l'ORTF (First Channel of the ORTF) when the ORTF was created on 25 July 1964. This period marked the introduction of commercial advertising on Première chaîne de l'ORTF which began on 1 October 1968. On 8 January 1969, the ORTF created a subsidiary company called Régie française de publicité (RFP) to handle all advertising on the ORTF channels.


=== TF1 (since 1975) ===
TF1 (which originally stood for Télévision Française 1 (French Television 1)), was created on 1 January 1975 when law 74-696 7 August 1974 (which split the ORTF into 7 organisations) came into effect, and the rebranding from Première chaîne de l'ORTF to TF1 came into effect on 6 January 1975. A new multicoloured logo for TF1 created by Catherine Chaillet came out that same year along with cel-animated idents, and from 1976 until 1985 analogue computer-generated idents produced using the Scanimate system were used on TF1, created by the American company Robert Abel and Associates with background music composed by Vladimir Cosma. The 1975 TF1 logo was later modified in 1984 and again in 1987. Colour television were first introduced to TF1 on 1 September 1975 when FR3 (now France•3) agreed to supply some of its colour programming to TF1, and the conversion to colour on TF1 was completed in late-1977. Since TF1's privatisation in 1987, the abbreviation is no longer expanded, so as to avoid confusion with the government-owned television broadcaster France Télévisions. The channel was also broadcast in Italy alongside La Chaîne Info on digital terrestrial television from 2004 to December 2006 on Dfree multiplex.


=== Logos ===

		
		


== Programmes ==
French TV shows now airing:

Julie Lescaut
Qui veut gagner des millions ?
Diane, femme flic
Alice Nevers, le juge est une femme
Joséphine, ange gardien
No limit
FalcoThese original TV shows were shown on TF1 :

Rose et Val, Les toqués, Soeur Thérèse.com, Mes amis, mes amours, mes emmerdes..., Commissaire Cordier, R.I.S Police scientifique, Les Cordier juge et flic, Extrême limite, La Vie devant nous, Navarro and MasterChef France.American TV shows now airing (dubbed and sometimes also subtitled; shown under their original titles except where indicated):

Arrow
Law & Order: Special Victims Unit (New York unité spéciale)
CSI: NY (Les experts : Manhattan)
CSI: Crime Scene Investigation(Les experts)
The Young and the Restless (Les feux de l'amour)
House(Dr House)
''Grey's Anatomy''
The Flash
The Following(Following)
The Mentalist (Mentalist)
Law & Order: Criminal Intent (New York section criminelle) CSI: Miami(Les experts : Miami)
Brothers & Sisters
Person of Interest
Criminal Minds (Esprits criminals) Nikita
The Blacklist(Blacklist)
Revenge
Ransom (Coming Soon)
BlindspotKids TV series

Totally Spies! (Original Productions)
The Amazing Spiez (Original Productions)
Monster Buster Club (Original Productions)
MasterChef France Junior (Original Programming)
F-Zero: GP Legend
Pokémon
Dora the Explorer
Gazoon (Original Programming)
Sonic Underground
Sonic X
Sugarbunnies
Sooty
Rekkit Rabbit (Original Programming)
Sesame Street
Babar and the Adventures of Badou (Co-Original Programming)
Pinky Dinky Doo
Phineas and Ferb
Miraculous: Tales of Ladybug & Cat Noir (Original Programming)
Marcus Level (Original Programming)
Saint Seiya (Les Chillvales Du Zordiaque)
Dragon Ball Z
Calimero 3D (Original Programming)In 2005, TF1 launched TF1Vision, a video on demand service.


== Criticism ==
Some commentators accuse TF1 of being an excessively populist, commercialised channel. There is a clear emphasis on "light" entertainment programmes over more serious content, and the channel's success is sometimes seen as being founded on the ménagères de moins de 50 ans (housewives under 50) audience segment. Certainly, a large proportion of the schedule consists of gameshows, sensational documentaries and dubbed versions of TV series. The channel's news service is perceived as consisting of more celebrity news and human-interest stories than its public-sector competitors.
On 16 April 2009, the employee responsible for the "Web innovation" department was fired for criticizing the HADOPI law in a private email (on 19 February) sent to a Member of Parliament. The management of TF1 was notified about the e-mail by the Ministry for Culture and Communication, whom Ministry Christine Albanel is also one the authors of the HADOPI law.In 2004 Patrick Le Lay, CEO of TF1 made the following statement about the channel's aims:

There are many ways to speak about TV, but in a business perspective, let's be realistic: at the basis, TF1's job is helping Coca-Cola, for example, to sell its product. What we sell to Coca-Cola is available human brain time. Nothing is more difficult than obtaining this availability. This is where permanent change is located. We must always look out for popular programs, follow trends, surf on tendencies, in a context in which information is speeding up, getting manifold and trivialized.
Critics of TF1 also contend that its news coverage is slanted towards supporting right-wing politicians – they were in particular accused of supporting Édouard Balladur in the 1995 presidential elections, and of overstating crime during the 2002 electoral campaign to tilt the balance in favour of former French president Jacques Chirac, who campaigned on a law and order platform.
Key figures within TF1 are close friends to some of the most powerful politicians in France, and the relationship between Bouygues and the public-sector contracting system often raises suspicions. Nicolas Sarkozy (president between 2007 and 2012) is a frequent guest of the channel, and is seen as being given an easier ride than on other networks. Immigration and violence are arguably conflated in the channel's news programmes. In addition, it is occasionally alleged that news reports from TF1 tend to ignore issues yielding a bad light on their parent group (Bouygues), while stressing the problems of competitors (such as Vinci SA).
Such criticism is heavy in the satirical show Les Guignols de l'info, broadcast on rival network Canal+. However, TF1 now competes in this category with M6, which was initially a generalist channel focusing on musical programmes, but now has programming more resembling TF1 (notably, reality shows that TF1 started running just after M6 introduced them).


== See also ==
List of television stations in France
TF1 Tower


== References ==


== External links ==
Official Site (in French)