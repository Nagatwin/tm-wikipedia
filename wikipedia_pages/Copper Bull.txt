The Copper Bull is a copper sculpture found at the site of Tell al-'Ubaid near the ancient city of Ur, now in southern Iraq,  by Sir Leonard Woolley in 1923. The sculpture, which dates from about 2600 BCE, is now in the British Museum.


== Discovery ==
The sculpture was found with a number of other artifacts at the base of a foundation in Tell al-'Ubaid. The sculpture was found by Leonard Woolley who was working jointly for the University of Pennsylvania Museum of Archaeology and Anthropology and the British Museum. The foundation which hid the sculpture was a platform made from brick and mud which had originally supported a temple to the goddess Ninhursag. The bull sculpture had been crushed by the falling masonry of the damaged temple. Woolley found similar models of bulls but only this and one other were recovered in an intact state.
Ninhursag was a goddess of the pastures, so it is appropriate that cows should be found around her temple; it was said that her milk sustained the kings.


== Construction ==
The sculpture had been made by first making a wooden model of a bull. This had then been coated with bitumen. The parts of the bull were made in sections. The legs were shaped out of wood covered in copper plate and held in place using tacks. The legs were attached and then more copper sheet covered the main body, with a different sheet for the bull's shoulders. These sheets were held in place by flat-headed nails along the back of the legs, the haunches and the stomach. The body's sheets overlapped the places where the legs had been attached.Copper bolts attached the legs to the base and the head was attached with a wooden dowel. The horns and the ears were then attached to the head.


== Recovery ==
By the time the bull was discovered the wood had decayed away and the bull was recovered only by casting wax around the fragile remains and then lifting the remains out for later conservation. The sculpture as exhibited in the British Museum includes a horn, part of the tail and hooves which have been recreated to make the bull complete.


== Ownership ==
The Copper Bull is in the British Museum. The artifacts discovered by Woolley were shared between the Iraqi State which received around 50% of the objects and the British Museum and the University of Pennsylvania Museum of Archaeology and Anthropology which each received about 25% of the objects. Ur was one of the first sites excavated under Iraq's antiquity law, drawn up by Gertrude Bell.


== References ==


== Bibliography ==
H.R. Hall and C.L. Woolley, Ur Excavations, vol. I: Al-Uba (London, Oxford University Press, 1927)
T.C. Mitchell, Sumerian art: illustrated by objects from Ur and Al-'Ubaid (London, The British Museum Press, 1969)